pipeline {
    agent any
    stages {
        stage ('pull') {
            steps {
                git 'https://github.com/shubhamkalsait/studentapp-ui.git'
            }
        }
        stage ('build') {
            steps {
                sh '/opt/apache-maven/bin/mvn clean package'
            }
        }
        stage ('test') {
            steps {
               withSonarQubeEnv(installationName: 'sonar-server', credentialsId: 'id_secrate') {
                    sh '/opt/apache-maven/bin/mvn clean package sonar:sonar -Dsonar.projectKey=newstudent-app}'
                }
            }
        }
        stage {'deploy'}
    }
}